import urllib
from BeautifulSoup import BeautifulSoup as bs
from flask import Flask

##############
# my func
##############
def get_tpb_html(tpb_url):
    f = urllib.urlopen(tpb_url)
    html = f.read()
    f.close()
    return html

def get_links(html,attrs=None):
    """ get everything that's a link in the given html, with the specified attributes """
    tpb_soup = bs(html)
    torr_links = tpb_soup.findAll('a',attrs=attrs)
    return torr_links

def refine_links(links_list):
    """ clean up the links by prepending the rest of the url """
    to_prepend = "http://thepiratebay.se"
    refined_links = []
    for i in range(len(links_list)):
        link_href = links_list[i].attrMap['href']
        refined_links.append(to_prepend+link_href)
    return refined_links


################################
# build the html to return here
################################
app = Flask(__name__)
@app.route("/")
def hello():
    # initializing stuff
    return_string = ""
    tpb_url = "http://thepiratebay.se/browse/205/0/7"

    # get the html we need from the website
    tpb_html = get_tpb_html(tpb_url)
    return_string += "<h1>I am going to do something now</h1>\
            <p>Maybe later I will even ask for your input</p>\
            <p>Going to wget http://thepiratebay.se/browse/205/0/7</p>\
            <hr>\
            <div style='color:gray; font-size:.2em; font-style:italic;'>%s</div>" % tpb_html

    # tell beautifulsoup we want links (a's), but only with a certain class (so not links on the page that don't point to torrents)
    links = get_links(tpb_html,attrs={'class':'detLink'})
    return_string += "<h3> Now I am going to pull some stuff out of the html -- just everything that's a link, with class = detLink:</h3>%s" % links

    # the urls in the links weren't absolute (href="/torrents/blah"), so prepending the "http://thepiratebay.se" part
    new_links = refine_links(links)
    return_string += "<h3> Now I am making the links actually work: </h3>%s" % new_links

    # Now here are just the text of each link
    return_string += "<h3> Now just the text of each </h3>"
    for link in links: 
        return_string += "<p>%s</p>" % link.text

    # and done
    return return_string


##############
# main!
##############
if __name__ == "__main__":
    app.run(debug=True)
